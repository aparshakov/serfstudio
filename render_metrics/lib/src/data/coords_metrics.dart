/// Coordinate class
class CoordsMetrics {
  final double y;
  final double x;

  CoordsMetrics({this.y, this.x});
}
